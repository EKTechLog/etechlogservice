package com.etechlogservice.springservice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;

class TechLogEntry {
	   String flightNo;
	   String tailNo;
	   String flightDate;
	   String flightTime;
	   String issues;
	   String defferedDefect;
	   String engineer;

	   public String getflightNo() {
	    return flightNo;
	   }
	   public void setflightNo(String flightNo) {
	     this.flightNo= flightNo;
	   }
	   
	   public String getTailNo() {
		    return flightNo;
		   }
	   public void setTailNo(String tailNo) {
		    this.tailNo= tailNo;
		   }
		   
	   public String getflightDate() {
			    return flightDate;
			   }
	   public void setflightDate(String flightDate) {
			    this.flightDate= flightDate;
			   }
	   public String getflightTime() {
				    return flightTime;
				   }
		public void setflightTime(String flightTime) {
				    this.flightTime= flightTime;
				   }
		public String getissues() {
		    return issues;
		   }
		   
		public void setissues(String issues) {
		    this.issues= issues;
		   }
		   
		public String getdefferedDefect() {
			    return defferedDefect;
			   }
			   
		public void setdefferedDefect(String defferedDefect) {
			    this.defferedDefect= defferedDefect;
			   }
		public String getengineer() {
		    return engineer;
		   }
		   
	    public void setengineer(String engineer) {
		    this.engineer= engineer;
		   }
} 