package com.etechlogservice.springservice;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class TechLogController {
	
	private static final Logger logger = LoggerFactory.getLogger(TechLogController.class);
	
	@org.springframework.beans.factory.annotation.Autowired 
	private TechLogRepository techLogRepository;

	@RequestMapping(value = "/read/log", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getAllLogs() {
	    return "Get All Logs";
	}
	
	@RequestMapping(
			  value = "/read/log/{flightno}", 
			  method = RequestMethod.GET, 
			  produces = "application/json"
			  )
			@ResponseBody
			public String getLogByFlightNo(
			  @PathVariable String flightno) {
		         return "Get Log By Flight No" + flightno;
			}
	
	@RequestMapping( 
			  value = "/read/log/", 
			  params = { "flightno", "date" , "time"}, 
			  method = RequestMethod.GET,
			  produces = "application/json"
			  )
			@ResponseBody
			public List <TechLog> getLogByFlightDateTime(
			  @RequestParam("flightno") String flightno, @RequestParam("date") String date, @RequestParam("time") String time ) {
				List <TechLog> log = new ArrayList<TechLog>(); 
				log = techLogRepository.findByFlightNo(flightno);
			    return log;
			}
	
	@RequestMapping(
			  value = "/write/log", 
			  method = { RequestMethod.PUT, RequestMethod.POST },
			  produces = "application/json"
			)
			@ResponseBody
			public String createNewLog(@RequestBody TechLogEntry log) throws Exception {
			    
				TechLog entity = new TechLog();
				entity.setflightno(log.flightNo);
				entity.setTailNo(log.tailNo);
				entity.setFlightDate(entity.stringToDate(log.flightDate));
				entity.setFlightTime(entity.stringToTime(log.flightTime));
				entity.setissues(log.issues);
				entity.setDefferedDefect(log.defferedDefect);
				entity.setengineer(log.engineer);
				//logger.debug("Requesting " + log.getflightNo() + ":" + log.getTailNo() );
				//return "Creating New Tech Log" + log.getflightNo() + log.getTailNo();
				techLogRepository.save(entity);
				return "Creating New Tech Log for " + log.flightNo + "-" + log.flightDate + "-" +  log.flightTime ;
			}
	
	@RequestMapping(
			  value = "*", 
			  method = { RequestMethod.GET, RequestMethod.POST },
			  produces = "application/json"
			  )
			@ResponseBody
			public String allFallback() {
			    return "Fallback for All Requests";
			}
	
}
