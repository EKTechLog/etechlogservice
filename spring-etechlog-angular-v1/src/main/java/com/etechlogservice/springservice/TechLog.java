package com.etechlogservice.springservice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name="TECHLOG")
public class TechLog {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="logid")
	private int logid;
	
	@Column(name="flightno")
	private String flightNo;
	
	@Column(name="tailno")
	private String tailno;
	
	@Column(name="flightdate")
	private Date flightdate;
	
	@Column(name="flighttime")
	private Timestamp flighttime;
	
	@Column(name="issue")
	private String issue;
	
	@Column(name="deferdefect")
	private String deffereddefect ;
	
	@Column(name="engineer")
	private String engineer ;

	public int getLogId() {
		return logid;
	}

	public void setLogId(int logId) {
		this.logid = logId;
	}
		
	public String getflightno() {
		return flightNo;
	}

	public void setflightno(String flightno) {
		this.flightNo = flightno;
	}

	public String getTailNo() {
		return tailno;
	}

	public void setTailNo(String tailno) {
		this.tailno = tailno;
	}

	public Date getFlightDate() {
		return flightdate;
	}

	public void setFlightDate(Date flightdate) {
		this.flightdate = flightdate;
	}

	public Timestamp getflighttime() {
		return flighttime;
	}

	public void setFlightTime(Timestamp flighttime) {
		this.flighttime = flighttime;
	}
	
	public String getissues() {
	    return issue;
	   }
	   
	public void setissues(String issue) {
	    this.issue= issue;
	   }

	public String getDefferedDefect() {
		return deffereddefect;
	}

	public void setDefferedDefect(String deffereddefect) {
		this.deffereddefect = deffereddefect;
	}
	
	public String getengineer() {
		return engineer;
	}

	public void setengineer(String engineer) {
		this.engineer = engineer;
	}

	

	
	public String datetoString(Date recieveddate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(recieveddate);
	}
	
	public Date stringToDate(String recievedstring) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date dt = null;
		try {
			dt = dateFormat.parse(recievedstring);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		java.sql.Date sqldt = new java.sql.Date(dt.getTime()); 
		return sqldt;
		
	}
	
	public String timetoString(Timestamp recievedtime) {
		return recievedtime.toString();
	}
	
	public Timestamp stringToTime(String recievedstring) {
		return Timestamp.valueOf(recievedstring);
		
	}

}

