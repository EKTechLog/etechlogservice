package com.etechlogservice.springservice;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TechLogRepository extends CrudRepository<TechLog, Long> {

	List<TechLog> findByFlightNo(@Param("archivedfalse") String flightno);
	//List<TechLog> findByFlightDate(@Param("status") String date);

}
